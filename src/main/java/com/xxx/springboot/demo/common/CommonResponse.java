package com.xxx.springboot.demo.common;

public class CommonResponse<T> {

  private Boolean status = false;

  private Integer code = 200;

  private String message;

  private T data;

  public Boolean getStatus() {
    return status;
  }

  public CommonResponse<T> setStatus(Boolean status) {
    this.status = status;
    return this;
  }

  public Integer getCode() {
    return code;
  }

  public CommonResponse<T> setCode(Integer code) {
    this.code = code;
    return this;
  }

  public String getMessage() {
    return message;
  }

  public CommonResponse<T> setMessage(String message) {
    this.message = message;
    return this;
  }

  public T getData() {
    return data;
  }

  public CommonResponse<T> setData(T data) {
    this.data = data;
    return this;
  }
}
