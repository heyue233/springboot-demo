package com.xxx.springboot.demo.entity;

public class User {

  private Long id;

  private String username;

  private String password;

  private String birth;

  private String phone;

  public Long getId() {
    return id;
  }

  public User setId(Long id) {
    this.id = id;
    return this;
  }

  public String getUsername() {
    return username;
  }

  public User setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public User setPassword(String password) {
    this.password = password;
    return this;
  }

  public String getBirth() {
    return birth;
  }

  public User setBirth(String birth) {
    this.birth = birth;
    return this;
  }

  public String getPhone() {
    return phone;
  }

  public User setPhone(String phone) {
    this.phone = phone;
    return this;
  }
}
