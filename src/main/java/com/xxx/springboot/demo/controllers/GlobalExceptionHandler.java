package com.xxx.springboot.demo.controllers;

import com.xxx.springboot.demo.common.CommonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
public class GlobalExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler(Exception.class)
  public CommonResponse<Object> handleException(Exception e) {
    LOGGER.error("全局异常处理：{}", e.getMessage());
    CommonResponse<Object> response = new CommonResponse<>();
    response.setStatus(false);
    response.setData(null);
    if (e instanceof NoHandlerFoundException) {
      response.setCode(404);
      response.setMessage("地址没有找到");
    } else {
      response.setCode(500);
      response.setMessage(e.getMessage());
    }
    return response;
  }
}
