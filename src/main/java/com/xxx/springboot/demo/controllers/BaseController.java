package com.xxx.springboot.demo.controllers;

import com.xxx.springboot.demo.entity.User;
import com.xxx.springboot.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaseController {

  private UserService userService;

  @Autowired
  public BaseController setUserService(UserService userService) {
    this.userService = userService;
    return this;
  }

  @GetMapping("/getUser")
  public User getUser(@RequestParam Integer id){
    return userService.getById(id);
  }

  @GetMapping("/getUserByName")
  public User getUser(@RequestParam String name){
    return userService.getByName(name);
  }
}
