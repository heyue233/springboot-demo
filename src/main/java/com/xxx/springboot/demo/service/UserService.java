package com.xxx.springboot.demo.service;

import com.xxx.springboot.demo.entity.User;

public interface UserService {

  User getById(Integer id);

  User getByName(String name);
}
