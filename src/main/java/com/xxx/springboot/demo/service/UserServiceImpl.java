package com.xxx.springboot.demo.service;

import com.xxx.springboot.demo.entity.User;
import com.xxx.springboot.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class UserServiceImpl implements UserService{

  private UserMapper userMapper;

  @Autowired
  public UserServiceImpl setUserMapper(UserMapper userMapper) {
    this.userMapper = userMapper;
    return this;
  }

  @Override
  public User getById(Integer id) {
    return userMapper.getById(id);
  }

  public User getByName(String name){
    return userMapper.findByName(name);
  }
}
