package com.xxx.springboot.demo.mapper;

import com.xxx.springboot.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {

  User getById(Integer id);

  @Select("select * from tb_user u where u.username = #{name}")
  User findByName(String name);

}
